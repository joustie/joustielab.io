---
layout: post
title: "2016 - the next big thing?"
date: 2016-01-02 10:38:26
categories: agile bachelor devops family javascript stonehenge vacation
---

So now we are a day forward in the new year and I realized I’d like to say something about it. For me 2015 has been a big year: finished my studies, learning new things about javascript, cloud business, apps, Agile and DEVOPS, amazing vacationing to Portugal with my love, a couple of weeks in the U.K. with the family in the summer, getting to run 10 KM …  a lot of things.

![Stonehenge gathering](/images/2016/01/IMG_0666.jpg)

Thinking about the past, the future

So what will 2016 bring.. ? I hope more knowledge, challenges, love and fun and more health! *All the best!*
