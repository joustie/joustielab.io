---
layout: post
title: "Agile methods and technologies for  KPN HR-Analytics"
date: 2015-10-20 11:59:19
categories: agile bachelor loi research
---

This is a translation of the first part of the introduction of my thesis:

![Frontpage of thesis ](/images/2016/01/Screen-Shot-2015-10-20-at-13-57-32.png)
The need for relevant information to distinguish yourselves from others, making you faster than your competitor, has existed since the creation of the first societies. In the course of recorded history more and more data accumulated in different administrations. Since the advent of the computer and the use of databases, there are all kinds of new possibilities to analyze this data and transform it into useful information. Soon the management of large organizations (government and industry) acknowledged the usefulness of these analyses.

The systems and applications for this analysis were called Management Information Systems (MIS). In the last decade, features were added to these systems, which enabled aggregation and different ways to present information. The data is collected in a so-called data warehouse.  
 Since the expansion of functionality, these systems are known as Business Intelligence (BI) systems. They are indispensable for organizations in business and government to support the management in strategic and tactical issues.

Yet there is growing amount of data stored within organizations and with a tremendous pace. This is due to an ever-increasing external environment in which organizations (are able to) operate. (Aldrich & Mindlin, 1978). Think of blurring boundaries, faster communication, in short: globalization. The processing of this data stream will have to take place more rapidly in order to maintain the competitive advantage (Choo, 1995).

The design and construction of these systems requires usually a lot of time and money. The potential revenue when introducing such systems are high but research shows that there is large gap between investing in a better BI environment and reaping the benefits of it (Williams and Williams, 2007). Unfortunately half to one third of the data warehouse development projects fail. (Hayen et al., 2007) One of the reasons is the complexity of the systems and the data to be processed. There are various techniques involved and the requirements are often unclear. A possible consequence of this failure is that companies have fewer good reporting capabilities. They lack insight into their own activities and are less successful.

The fact that many BI projects fail is very unfortunate because in recent years much progress has been made in the mainstream software development industry through the use of agile development methodologies. These emphasize the continuous delivery of new functionality, intensive communication between customers and the development team and a total focus on quality. The Agile principles can also be applied to BI development projects but in the real world this is not common yet.  
 Several authors have written on this subject and argue that it is a misconception that agile principles are not applicable to BI (K.Collier, 2011). The best practices that exist and grew out of these principles can be used in modified form.  
 This thesis examines whether this also applies to KPN’s HR BI environment.


