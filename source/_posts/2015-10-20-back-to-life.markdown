---
layout: post
title: "Back to life"
date: 2015-10-20 11:26:38
categories: 8800gs apple chipset imac nvidia overheating reballing
---

A month ago or so I turned on my iMac and something weird happened. The screen stayed black as oil. Reset it and it did actually start to boot, but after a couple of minutes….. bam.. black screen. I could hear the fans still working but the machine died. Did some resets of NVRAM and stuff like that and I was able to boot to the console. Unfortunately, after examining the logfiles I found kernel panics loading the nvidia driver. This pointed to graphics card problems. After some research on the web I found that my iMac’s gpu (Nvidia 8800GS) is known for overheating problems. Must say I never experienced this before, but apparently all of those cards eventually succumb.

After calling Apple support they let me send the iMac to the nearby store for repair. A week later the dreaded answer: ‘ We do not supply this card anymore, so we cannot fix your Mac’.

This was quite a blow, since I really love this hardware. They even wanted me to pay for examining the iMac, but fortunately a Apple Support manager offered reimbursement by Apple.

So what then? I found a [company](http://www.ebay.nl/itm/Apple-iMac-24-A1225-Nvidia-Video-Graphics-Card-661-4664-Repair-8800-GS-Reball-/290766866098) on the internet which made repairing chipsets it’s business. Still, quite a gamble, as it is still about 230 euro’s. But it was my last change to get the iMac working again. Remember, the form factor of this card is so specialised, it is along with an obscure ATI card the only one that will fit into the casing of an iMac attached to the the logic board.

<div class="wp-caption alignleft" id="attachment_258" style="width: 160px">![Screen removed](https://docs.google.com/uc?id=0B6Wt_WnMwIKqNEZiUkl5VGFwYVU&export=download)Screen removed

</div><div class="wp-caption alignleft" id="attachment_250" style="width: 160px">[![Bezel removed](https://docs.google.com/uc?id=0B6Wt_WnMwIKqc0NrUUdVYy1ubGc&export=download)](http://i0.wp.com/www.joustie.nl/wp-content/uploads/2015/10/IMG_1119-e1445338155621.jpg)Bezel removed

</div><div class="wp-caption alignleft" id="attachment_253" style="width: 160px">[![LCD off](https://docs.google.com/uc?id=0B6Wt_WnMwIKqS21lU0Q1UU9aa1U&export=download)](http://i1.wp.com/www.joustie.nl/wp-content/uploads/2015/10/IMG_0381-e1445338091418.jpg)LCD off

</div><div class="wp-caption alignleft" id="attachment_249" style="width: 160px">[![Naked imac](https://docs.google.com/uc?id=0B6Wt_WnMwIKqS3ZfYlE3OTl4dW8&export=download)](http://i2.wp.com/www.joustie.nl/wp-content/uploads/2015/10/IMG_0247.jpg)Naked imac

</div><div class="wp-caption alignleft" id="attachment_255" style="width: 160px">[![Logics board with video card attached](https://docs.google.com/uc?id=0B6Wt_WnMwIKqaWNnYWR4R3JhR3M&export=download)](http://i2.wp.com/www.joustie.nl/wp-content/uploads/2015/10/IMG_1123.jpg)Logic board + videocard

</div><div class="wp-caption alignleft" id="attachment_248" style="width: 160px">[![The defect card](https://docs.google.com/uc?id=0B6Wt_WnMwIKqcUFvaVFQYURDWDg&export=download)](https://docs.google.com/uc?id=0B6Wt_WnMwIKqcUFvaVFQYURDWDg&export=download)The defect card

</div>- - - - - -

 

 

 

 

 

 

 

 

 

So I had to disassemble the entire iMac to get the graphics board out of it. This was quite an endeavour, because Apple used some special Torque screws and clever engineering. But I succeeded and created a package with the videocard to be sent to the UK.

After a week or so it returned, supposedly fitted with a brand new GPU (graphics processing unit). A bit nervous and hopeful, I reassembled the iMac.

<div class="wp-caption alignleft" id="attachment_261" style="width: 160px">[![Reassemble!](https://docs.google.com/uc?id=0B6Wt_WnMwIKqQVBvUFNSUDJMUTg&export=download)](http://i1.wp.com/www.joustie.nl/wp-content/uploads/2015/10/IMG_0380-e1445339549478.jpg)Reassemble!

</div><div class="wp-caption alignleft" id="attachment_251" style="width: 160px">[![Booting...](https://docs.google.com/uc?id=0B6Wt_WnMwIKqdURsNUFlaUhka28&export=download)](http://i1.wp.com/www.joustie.nl/wp-content/uploads/2015/10/IMG_0385.jpg)Booting…

</div><div class="wp-caption alignleft" id="attachment_252" style="width: 160px">[![My old desktop](https://docs.google.com/uc?id=0B6Wt_WnMwIKqWnNTVDRzRVpDMU0&export=download)](http://i1.wp.com/www.joustie.nl/wp-content/uploads/2015/10/IMG_0387-e1445338024710.jpg)My old desktop!

</div> 

 

 

 

 

It works!! Thanks Haytek LTD!!


