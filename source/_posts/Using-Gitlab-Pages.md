---
title: Using Gitlab Pages
date: 2017-10-03 23:35:36
tags:
---
Working professionally everyday with Gitlab has made me think twice of hosting my blog on an isolated Digital Ocean Droplet. Especially as we are promoting Devops and Continuous Delivery (for which I've been attending a training by Jez Humble last week!)

So I've converted my blog to use Gitlab Pages. This means my entire blog resides in texfiles in Gitlab(.com) and whenever I push a new version of the code, the Gitlab runners build my blog using a Static Site Generator (Hexo).This sounds confusing I know, but I will explain in the coming week.

![Blog Pipeline](/images/2017/10/new_post.PNG) 
