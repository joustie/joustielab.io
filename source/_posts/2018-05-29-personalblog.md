---
title: A bit more personal
date: 2019-01-12 13:06:00
tags: personal
---
So, this is 2019. What is changing for me? Wel.. I am not going to spill al of my life over here, but I am planning to blog more.  

I totally agree with Jaron Lanier that the current social media are flawed. They are based on the 'freemium' model using adverts. He argues they are not 'social' anymore as we tend to see them but they have become 'behavioral modification empires'. In his behavorial model the negative impulses get more rewarded than the positive ones, and this is bad. 
I personally think this also is true for the schoolyard or playground, so maybe this it is a bit idealistic to think we can limit this.
 
He also thinks it is not too late. We can correct these [mistakes from the 90's.](https://www.amazon.com/Arguments-Deleting-Social-Media-Accounts/dp/125019668X/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=0520271440&linkCode=as2&tag=teco06-20)
Here is his TED-talk about this: https://www.ted.com/talks/jaron_lanier_how_we_need_to_remake_the_internet. We can delete our accounts and use personal sites or paid sites or other ways of celebrating culture and creativity (his words). Or remove the advertisement models, let people pay for things. We don't need a situation where for two people to communicate a third entity is needed to facilitate this and at the same time manipulates this communication.
 
So... Let's make this reality, bringback the blog , making it personal. New ways to connect just like the development of language. I am not as eloquent as Jared but I dig this idea. 
 
I do think we should cherry-pick features from the 'social media' and try to reinvent them differently. How do we like each other stories? In the blog era there was ping-back and rss. I am not sure if that is the answer. These things should be simplified if we want people to switch.
What about 'new social' networks that promise to protect your privacy better? A new peer-to-peer system to connect personal blogs? The v3 web? 

A technique that could aid in this is being developed by Tim Berners Lee new [initiative](https://www.inrupt.com/blog/one-small-step-for-the-web). It aims to safeguard your privacy. The product they are building is called Solid:
'Solid changes the current model where users have to hand over personal data to digital giants in exchange for perceived value.'

If social media or blogs use this model to protect our sensitive data the world would become a better place!

New social media
- [Mewe](https://mewe.com)

New techniques to create media
- [Solid](https://solid.inrupt.com/)

Just some links about blogs
- [Return of the blogroll](https://hiddedevries.nl/en/blog/2019-01-02-return-of-the-blog-roll)
- [Example of nice blogs](https://makeawebsitehub.com/examples-of-blogs/)

