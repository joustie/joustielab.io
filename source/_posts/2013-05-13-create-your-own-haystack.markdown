---
layout: post
title: "Create your own haystack"
date: 2013-05-13 21:11:09
categories: ldap php windows
---

So I’d like to introduce Stackoverflow-like functionality to my company network. It can support us internally, because we run a front-and back office team. But we also talked about integrating solutions like this into our own BI/MDM product: [My Cloud Controls.](http://www.cloudseven.nl/my-cloudcontrols/ "My Cloud Controls")

We can buy us some of the functionality (from the cloud for instance, or outsource it otherwise), but at our place, we’d like to build or run our own servers. So let’s give it a try. I actually already did by investigating [this list](http://meta.stackoverflow.com/questions/2267/stack-overflow-clones "List of SO clones") 2 months ago.

The first candidate we’re testing is [Question2Answer](http://www.question2answer.org "Q2A"). We like it, but it is a bit .. spartan (is that english?).


