---
layout: post
title: "Mobile testing with Appium (and later Sauce Labs)"
date: 2016-01-17 21:41:01
categories: 
---
Building a Continuous Delivery flow was not our only goal. Getting automated testing in there would be very helpful. So the first idea was to get visual testing going using Appium. That took me a day or so, but we got it working.

![Appium startup screen](/images/2016/01/appium_fixed_final.png)

The tests are very simple and for now only prove that tapping or clicking works, the assertions are not finished yet. Tests are written in ‘Mocha’ style and can be executed from the command line and as a job on our Jenkins servers later on :

```language-javascript
it("should contain a button for logging in ", function () {
    return driver
           //find the login button
      .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAButton[1]")
          .should.eventually.exist
      });

it("a succesful login should be followed by a list of projects", function () {
    return driver
          //click the login button
      .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAButton[1]").click().sleep(1000)
      //the row with a project should show up
      .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAStaticText[10]")
          .should.eventually.exist
      });

 it("should be able to get details of project", function () {
    return driver
           //click the row of the project
      .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAStaticText[9]").click()
          //the project name field should appear
       .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAStaticText[3]")
          .should.eventually.exist
      });

 it("should be able to set this project in edit mode and save state", function () {
    return driver
      //click the aanpassen area
      .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAButton[2]").click()
      //click the name input field to make it editable
      .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIATextField[1]").click()
      //wait a bit for client server communication
      .sleep(5000)
        //click save
          .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAButton[2]").click()
          //wait a bit for client server comm
       .sleep(5000)
      //go back to main project list
      .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAButton[1]").click()
      .sleep(5000)
      //project rows should be there
      .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAStaticText[10]")
          .should.eventually.exist
      });

it("should be able logout", function () {
    return driver
       //click the logoutbutton
           .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAButton[1]").click()
       //.sleep(1000)
           //click on yes
           .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAButton[5]").click()
           .sleep(1000)
       //a  row with a project should show up
                        .elementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[2]/UIAWebView[1]/UIAStaticText[10]").isDisplayed()
      });
```

Below is the inspector used to find out how controls can be identified by Xpath-value.
![Appium Inspector](/images/2016/01/appium_inspector_final.png)
