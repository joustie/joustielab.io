---
title: Down and out in Katowice
date: 2017-05-13 22:23:18
tags:
---
{% codeblock [First code] [lang:bash]  %}
var express = require('express', template = require('pug'));
var session = require('express-session')
var util = require('util');
var oauth = require('oauth');
{% endcodeblock %}

 
So what is this? It is the first code I used for the backend part of our project at the [ING Hackathon 2017](https://24hcoding.com/) in Katowice, Poland. It was held at Walcownia Cynku,a former Zinc rolling Mill (big scary machines!).

![logo](/images/2017/05/IMG_2821.JPG)
*The logo*

I was part of a team of 6 people and we had a great time. We arrived thursday afternoon and had a nice evening (I also met our colleagues from the CDaaS support team who are based in Katowice)
![teampic](/images/2017/05/IMG_2830-1.JPG)

*The countdown* 

Unfortunately our team did not win the competition, but one of us won a prize for best photo's and it was a great experience above all!

![wal1](/images/2017/05/IMG_2870.JPG)
*The venue*

![wal2](/images/2017/05/IMG_2835-1.JPG)
*Don't go near this!*

