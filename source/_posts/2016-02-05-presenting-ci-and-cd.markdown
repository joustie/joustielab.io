---
layout: post
title: "Presenting CI and CD"
date: 2016-02-05 20:15:36
categories: agile cd ci jenkins vagrant selenium
---
Last week I have done a presentation to explain to colleagues how they can use the CI and CD infrastructure I have put up for us. 

The image links to the presentation:

[![CSCI](/images/2016/02/csci.png)
](https://docs.google.com/uc?id=0B6Wt_WnMwIKqd18wV1d3SEdMbEk&export=view)