---
layout: post
title: "Go supersecure"
date: 2015-09-18 15:10:32
categories: emcads encryption enterprise-mobility excitor gon vpn
---
At Cloud Seven we endorse the super secure G/On product. It replaces the VPN technology that is commonly used to deliver remote access to the company network to employees. The easy part is you just plug in the USB stick and boot your computer and login, choose your application and it starts automatically and connects to your company’s servers and networks!

![gon stick](/images/2016/01/IMG_0180.jpg)
*G/ON USB stick*

This product was acquired by [Excitor](https://www.excitor.com) in 2012, the creators of [DME](http://www.excitor.com/your-work-stays-inside-secure-mobile-workplace) and integrated G/On in their mobile app as the [Appbox feature](http://www.excitor.com/HTML5-Secure-Platform). (Mobile Secure Email synchronization and remote Secure Apps).  
 It was recently selected to secure English [councils](http://www.computerworlduk.com/news/security/councils-ditch-vpn-start-using-g-on-secure-remote-access-3615457/) in the U.K.

It is very easy to implement, uses industry grade encryption for connections and links up to your Active Directory or LDAP server for authentication.


