---
layout: post
title: "Creating Excel sheets with java"
date: 2016-10-22 08:10:12
categories: 
---
Currently I am working on a project where we need to create Excel reports from a datasource. There are several ways to do this programmatically, but our first  thought was to do this in Java. We actually abandoned this path later on and went for something with Nodejs, but I'll share my thoughts on the Java road nonetheless.

There are several Java libraries available which enable read-write of Excel sheet formats.

But wouldn't it be great to have an Excel template that can be used by non-programmers to create a report and that your program can fill this with data?

Enter [Jxls](http://jxls.sourceforge.net), you can use this library to achieve this.

The default example shows how to create an Excel report with a collection of Java objects. 

I specifically wanted to know if it would be possible to iterate through child objects and create rows with those as well. Earlier versions of Jxls could not handle this the way I wanted. 

So I altered the demo a bit to answer my question. Maybe someone will benefit from this as well.

The Excel template:
![Excel template](/images/2016/10/excel_tempate.png)

The result:
![Excel report](/images/2016/10/Excel-report-1.png)

Here is a link to the [altered](https://github.com/Joustie/jxlsnestedobjectsdemo) demo on github.


 
