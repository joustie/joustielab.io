---
layout: post
title: "Almost done"
date: 2015-03-18 12:32:26
categories: agile bachelor loi
---

After 5 years the end is really near. Don’t worry, nothing serious  😉 What was I doing? I tried to combine family life with small children, work and a part-time bachelor studies.

So the last hurdles for me to become a Bachelor of ICT:

- finishing my thesis (or actually, refining it)
- finish last course  Service Level Management by writing a Service Level Agreement and answer some questions for the last assignment
- finish the last practical assignment by creating a prototype .NET website in VB with a new technology like a web service that can be used by an app


