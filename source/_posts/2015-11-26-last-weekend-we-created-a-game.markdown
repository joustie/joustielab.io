---
layout: post
title: "Last weekend we created a game..."
date: 2015-11-26 21:16:50
categories: 
---

It is called spookie! Doris and Tessel both contributed. [Click here to play and view it!](https://scratch.mit.edu/projects/88678568/#player)

![Scratch game screenshot](/images/2016/01/spelletje.png)
*Monsters all around!*

 
