---
layout: post
title: "The needle in the stack is quite big these days"
date: 2013-05-13 15:37:11
categories: experts-exchange gamification hp-forums jeff-atwood pcchips stack-overflow
---

A couple of years ago I was introduced to stackoverflow.com. That was quite an improvement over my frantic Google searches for IT related questions at the time. I did not like the[ experts-exchange site](http://www.experts-exchange.com/ "Experts-Exchange") which was very popular back then.

Long before that I even used old-school Usenet for asking and answering questions about IT topics. This was *before* it was primarily known as an alternative to file-sharing. The funny thing is it actually [predates](http://en.wikipedia.org/wiki/Usenet "Usenet") peer-to-peer file-sharing of course.

I actually forgot some of the stuff I posted those days. I looked for it on Google groups and it happens to be still around…

As as student, obtaining good hardware was… a priority (not necessarily for gaming though, I just liked to tweak):

- [Re: pcchips motherboard identification](https://groups.google.com/forum/?hl=nl&fromgroups#!search/Joost$20Evertse/comp.sys.ibm.pc.hardware.chips/B3WxAfM-e9I/lXScIL_XPM0J)

And from my early sysadmin days:

- [Re: Lost emails via VPN connection!](https://groups.google.com/forum/?hl=nl&fromgroups#!search/Joost$20Evertse/microsoft.public.exchange.clients/j_ZNbvUKdNg/FvCdB4pVupEJ)
- [Re: Can two Ex servers use one OWA at the same time?](https://groups.google.com/forum/?hl=nl&fromgroups#!search/Joost$20Evertse/microsoft.public.exchange.admin/M06BympDsgg/DLVHfZxcVuAJ)

Well, enough of that. Back to Stackoverflow. It was conceived by Jeff Atwood and Joel Spolsky in 2008. They wanted to create an open community driven source of knowledge based on the [Creative Commons ](http://en.wikipedia.org/wiki/Creative_Commons "Creative Commons")License. Here is how Jeff Atwood described it:

> **Stackoverflow is sort of like the anti-[experts-exchange](http://experts-exchange.com/) (minus the nausea-inducing sleaze and quasi-legal search engine gaming) meets [wikipedia](http://www.wikipedia.com/) meets [programming reddit](http://programming.reddit.com/).** It is by programmers, for programmers, with the ultimate intent of collectively increasing the sum total of *good* programming knowledge in the world. No matter what programming language you use, or what operating system you call home. Better programming is our goal.

I have been using the site from time to time, both passively harvesting information as well as contributing by answering questions.

The site uses a concept called ‘[Gamification](http://en.wikipedia.org/wiki/Gamification "Gamification")‘. Users can earn reputation points and lose them as well through voting. It is a measurement of your position in the community.

It became clear to me that the system behind it just works. Years ago (another century) I was HP forums user and it also uses the point system. It gives a warm fuzzy feeling when you belong to something bigger and can help people out.


