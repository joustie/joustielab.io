---
layout: post
title: "Jenkins cookbook for CHECK"
date: 2015-12-22 23:56:30
categories: net agile asp-classic ci devops jenkins oracle plsql
---

We are using [Jenkins](https://jenkins-ci.org) Continuous Integration Server for automating tasks that would otherwise take days and generate more errors surely. We  build the database server and web infrastructure daily for our most important application (legacy with asp classic,NET 2.0/3.5 parts and a lot of Oracle 11g PL/SQL components). When completed, a set of integration tests are run with selenium.
![Diagram of CHECK workflow](/images/2016/01/Continuous-Delivery-for-KPN-CHECK-1.jpg)
*Deployment workflow*


**How we came to this**

While developing on a dedicated test server for years we encountered integration problems frequently when deploying code to production. First we started using [Nant](https://en.wikipedia.org/wiki/NAnt) to create automated deployment. This way we could guarantee deploying the software the same way every time. The real problem was that the Oracle test database was starting to differ too much from the production site. To make sure we always used the same infrastructure I took up the laborious task of creating scripts for every object in the Oracle database and put them under version control. Finally, I also created all the scripts to create the Oracle database from the command line and bundled them with the object creation scripts.By then (december 2014) we had to migrate the entire application to another infrastructure provider, so I used the new scripts to create the new environment there from scratch. The servers are all virtual, using [Virtualbox](https://www.virtualbox.org) technology. For orchestration I used [Vagrant](https://www.vagrantup.com/about.html), the amazing tool with config files in [Ruby](https://en.wikipedia.org/wiki/Ruby_(programming_language)) style by [Mitchell Hashimoto](https://twitter.com/mitchellh), coupled with [Chef](https://www.chef.io) (also Ruby) for further automation and provisioning when the virtual machine is up and running.In march 2015  I created nightly build jobs in Jenkins to create the database and web infrastructures.

The steps in this build:

**Step 1: Deploying the database server**

* deploying a Vagrant Windows 2012 box that is somewhat preconfigured
* install the Chef client for windows  
* start a Chef provision run
* installing a telnet client
* installing .Net 3.5
* getting the database creation and configuration scripts from git
* installing Oracle 11g
* creating an Oracle database with several tablespaces
* creating an Oracle Listener
* creating Oracle schema’s (about 7)
* create database objects in Oracle (around 500 tables and 80000 lines of PL/SQL code and many more objects)
* populate the entire database with data
* refresh all indexes and snapshots and statistics

**Step 2: Deploying the web server**

* deploying a Vagrant Windows 2012 box that is somewhat preconfigured
* install the Chef client for windows
* start a Chef provision run - installing a telnet client
* installing .Net 3.5
* installing the IIS web server Role for Application development with ISAPI extensions, ASP.NET and such
* mapping a drive to a data volume
* installing an oracle client using a silent install with answer file
* configuring the oracle client by placing configuration files (tnsnames.ora and such)
* do a git clone of the custom asp code that runs on the server
* open the local firewall for some web ports
* set the timezone to CET
* disabling NLA for rdp
* configure IIS with a custom recipe : - creating and configuring an app pool for the application
* create a site to host the code
* configure the site with some debug attributes for development

**Step 3: Running integration tests**

* running Selenium tests which where recorded with firefox
* each test does: - login test
* use case test

**Step 4: Automated deployment (when the integration tests succeed)**

* creates a deployment zip package
* populates the zip with a file structure for the Acceptance environment and production environment
* runs a nodejs javascript to edit the config files and set them to the correct values in each environment directory structure
* updates the README in the main directory with instructions
* the zip package that is ready to be deployed is uploaded to our configuration management system


