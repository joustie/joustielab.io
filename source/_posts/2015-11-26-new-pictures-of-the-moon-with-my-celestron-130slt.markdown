---
layout: post
title: "New pictures of the moon with my Celestron 130SLT"
date: 2015-11-26 20:53:58
categories: moon nerd photo telescope
---



![The moon through a nice filter](https://docs.google.com/uc?id=0B6Wt_WnMwIKqeklPNGhoZ1BHTWM&export=download) *The moon through a nice filter*

![Relief in the dark](https://docs.google.com/uc?id=0B6Wt_WnMwIKqX2JyNzZiSUJuaEk&export=download) *Relief in the dark*

![Lots of shading and relief](https://docs.google.com/uc?id=0B6Wt_WnMwIKqVUdlTkVrLUhhcDQ&export=download) *Lots of shading and relief*

![Clean picture of the moon](https://docs.google.com/uc?id=0B6Wt_WnMwIKqN1RDR0hlSjFnMFE&export=download)*Clean picture of the moon*

![Alone in the dark](https://docs.google.com/uc?id=0B6Wt_WnMwIKqWF9oREIzQWZCeEk&export=download)*Alone in the dark*


