---
layout: post
title: "Up to speed"
date: 2015-09-16 10:56:04
categories: bachelor cloud ict javascript kpn loi react
---

Finally an update on my blog for you guys. So let me get you current on developments:

* I am now officially a Bachelor of ICT!

* I’m presenting the conclusion from the thesis at KPN soon

* the [code](https://gitlab.cloudseven.nl:8081/joost/horang) I used for my final assignment is downloadable at https://gitlab.cloudseven.nl:8081/joost/horang

* visited [XebiCon](https://xebicon.nl) 2015 in May and heard lots of interesting stuff on Agile development

* visited the [Microsoft Techdays](https://www.microsoft.com/netherlands/techdays/) and learned lots about Javascript, React and Cloud developments

* visited Paris to attend [React Europe 2015](https://www.react-europe.org/2015.html) and learned tons of stuff on React and javascript


