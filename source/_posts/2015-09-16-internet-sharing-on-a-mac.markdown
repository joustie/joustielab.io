---
layout: post
title: "Internet sharing on a Mac"
date: 2015-09-16 11:09:08
categories: apple os-x unix
---

I use a Mitel desk phone which connects to my company’s PBX when working from home. I used to hook it up to my iMac and used internet sharing to make it connect. I tweaked the default network of the internal DHCP server used (to avoid a clash with the 2.0 network at my company)  by adding a ‘SharingNetwork’ key to the NAT configuration in /Library/Preferences/SystemConfiguration/com.apple.nat

As of Yosemite this ceased to work and I used an extra wire to my switch to connect the Mitel.

I was certain it should work so I spent some time on google and found the solution [http://hints.macworld.com/article.php?story=20090510120814850](http://hints.macworld.com/article.php?story=20090510120814850):

<div></div><div>- - - - - -

</div><div><div class="devcodetools" style="width:100%;"><div style="float:left"> Bash | </div><div style="float:left;background-image:url('http://www.joustie.nl/wp-content/plugins/devformatter/img/devformatter-copy.png');background-repeat:no-repeat;background-position:50% 50%;width:16px;height:16px;"> <embed align="middle" allowfullscreen="false" allowscriptaccess="always" bgcolor="#ffffff" flashvars="id=7&width=16&height=16" height="16px" id="ZeroClipboard7" loop="false" menu="false" pluginspage="http://www.macromedia.com/go/getflashplayer" quality="best" src="http://www.joustie.nl/wp-content/plugins/devformatter/_zclipboard.swf" type="application/x-shockwave-flash" width="16px" wmode="transparent"></embed></div><div style="float:left"> copy code |</div><div onclick="devfmt_credits()" style="float:left;cursor:pointer" title="DevFormatter Plugin">?</div></div>  
<div class="devcodeoverflow"><table class="devcodearea" width="100%"><tr><td class="devcodelines" width="3%">1</td><td class="devcodelinesarea"><span style="color: #c20cb9; font-weight: bold;">sudo</span> defaults <span style="color: #c20cb9; font-weight: bold;">write</span><span style="color: #000000; font-weight: bold;">/</span>Library<span style="color: #000000; font-weight: bold;">/</span>Preferences<span style="color: #000000; font-weight: bold;">/</span>SystemConfiguration<span style="color: #000000; font-weight: bold;">/</span>com.apple.nat NAT <span style="color: #660033;">-dict-add</span> SharingNetworkNumberStart 192.168.20.0

</td></tr><tr><td class="devcodelines devcodelinesodd devcodelinesodd" width="3%">2</td><td class="devcodelinesarea devcodelinesareaodd devcodelinesareaodd"><span style="color: #c20cb9; font-weight: bold;">sudo</span> defaults <span style="color: #c20cb9; font-weight: bold;">write</span><span style="color: #000000; font-weight: bold;">/</span>Library<span style="color: #000000; font-weight: bold;">/</span>Preferences<span style="color: #000000; font-weight: bold;">/</span>SystemConfiguration<span style="color: #000000; font-weight: bold;">/</span>com.apple.nat NAT <span style="color: #660033;">-dict-add</span> SharingNetworkNumberEnd 192.168.20.20

</td></tr><tr><td class="devcodelines" width="3%">3</td><td class="devcodelinesarea"><span style="color: #c20cb9; font-weight: bold;">sudo</span> defaults <span style="color: #c20cb9; font-weight: bold;">write</span><span style="color: #000000; font-weight: bold;">/</span>Library<span style="color: #000000; font-weight: bold;">/</span>Preferences<span style="color: #000000; font-weight: bold;">/</span>SystemConfiguration<span style="color: #000000; font-weight: bold;">/</span>com.apple.nat NAT <span style="color: #660033;">-dict-add</span> SharingNetworkMask 255.255.255.0

</td></tr></table></div>

</div>
