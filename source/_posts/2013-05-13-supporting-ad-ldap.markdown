---
layout: post
title: "Supporting AD LDAP"
date: 2013-05-13 21:18:18
categories: ldap php windows
---

Almost forgot that I have made a contribution to the Question2Answer [plugin](https://github.com/zakkak/qa-ldap-login "qa-ldap-plugin") for LDAP-authentication. I need it to connect to MS Active Directory through LDAP and it did not work out of the box for me. After some tweaking I decided to fork the plugin on Github. They accepted my changes when I created a pull request, so that is cool.


