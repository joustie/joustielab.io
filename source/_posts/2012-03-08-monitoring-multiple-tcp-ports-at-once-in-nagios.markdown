---
layout: post
title: "Monitoring multiple tcp-ports at once in Nagios"
date: 2012-03-08 21:52:58
categories: linux monitoring nagios nagios-plugin perl
---

During the last two months I have been setting up [Nagios](http://www.nagios.org) as a side project. We have a lot of new customers coming in and we need to have a tight grip on our systems to satisfy service levels. So as I set up a Debian host to run Nagios on and was making progress in adding systems, I encountered several issues.

One of them was the fact that I need to monitor several tcp ports that are utilized in the applications we are running in a JBoss container. The default script in Nagios to monitor a tcp-port works well and I have used it to create a custom plugin to monitor several ports sequentially.

I have defined a service for this in Nagios and you can see this service is applied to a particular host group:
<pre>
define service {   
    hostgroup_name  dme−servers 
    service groups DME_DIENSTVERLENING 
    service_description check_tcp_ports 
    check_command check_tcp_multiport!1098,1099,4444,5011,5021 
    use generic−service 
}
</pre>
  
 Below is the code I have put in  
/usr/lib/nagios/plugins/check\_tcp\_multiport.pl

<pre>#!/usr/bin/perl -w
use strict;
use Getopt::Long;

my ($plugin_home,$hostname,$ports);
$plugin_home = "/usr/lib/nagios/plugins/";
getoptions();

sub getoptions
{
      my @ports;
      my $error_count = 0;
      my $error_ports ="";
      my $result=GetOptions
      (
              "h|host=s"=>\$hostname,
              "p|port=s" => sub
              {
                      $ports = pop,
                      @ports = split(/,/,$ports)
              },
      );
      foreach my $port (@ports)
      {
        my $output = `$plugin_home/check_tcp -H $hostname -p $port`;
        my $exit_code = $?;
        chomp($output);
        if  ($exit_code != 0)
        {
            $error_count++;
            $error_ports .= "$port,";
        }
      }
      if ( $error_count == 0)
      {
              print "All ports are OK on $hostname\n";
              exit 0;
      }
      chop($error_ports);
      print "There are $error_count errors on $hostname: $error_ports\n";
      exit 2;
}
</pre>