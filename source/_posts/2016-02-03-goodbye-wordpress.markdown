---
layout: post
title: "Goodbye Wordpress"
date: 2016-02-03 22:23:18
categories: 
---
My blog has been running on the Wordpress platform for years. During the last year my development focus has been partly learning javascript and mobile development. As such, the time has come to put that knowledge to use in my personal projects. In the last couple of weeks I have migrated the old Wordpress platform to a new platform: [Ghost](https://en.wikipedia.org/wiki/Ghost_(blogging_platform)) It is pure javascript and is more focussed on pure publishing than Wordpress. What do you think?