---
title: Together in one world
date: 2018-05-29 22:35:36
tags:
---
Last 2 weeks it was 'ateliermiddag' again at the Prins Mauritschool in Rijswijk. These events are organised multiple times per year and the pupils of the school can do various small workshops.

Together with other dads I've lready given a Scratch 'programming' course a few times in recent years. With an emphasis on the creative side to ensure that the younger pupils could join as well. It's always been a great success.

This year we have come up with a new idea. Working together with a group of pupils in a central Minecraft world to build a model of the school!

![school](/images/2018/5/school.png)
*One of the models*