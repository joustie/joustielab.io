---
layout: post
title: "DEVOPS tools of the trade"
date: 2015-12-06 09:00:47
categories: agile bamboo ci devops jenkins teamcity tfs
---

One of the important concepts that came to DEVOPS from the Extreme Programming guys was Continuous Integration. It means you build and assemble together daily all the software that has been developed for your product and runs tests if it all still works.

![Diagram CD](/images/2016/01/Continuous-Integration-300x151-1.png)
*CI workflow*

When someone made an error or introduced a sneaky bug that breaks the product (we call it ‘regression’), it is immediately clear and this way it can be handled before shipping.  
 I really like this idea (as a system engineer) and I have introduced this concept to our software some years ago. Although many tools for build automation and integration exist let me first mention the (real) big ones. I will explain which one we used in a new post.

######  [Jenkins](https://jenkins-ci.org)

A server deployed in a Tomcat servlet container for running automated builds and the plugins and workflow structure allow you to connect to Configuration management tools, testservers, name it.. You can hook it up to your desired versioning system like git or svn. For instance, you can trigger a build when a developer commits new code and build the source code and run tests afterwards (or deploy to some environment). This has been around for some time and their plugin system is quite rich. You can integrate *anything* with Jenkins.

###### [Teamcity](https://www.jetbrains.com/teamcity/)

Created by Jetbrains who also make beautiful IDE’s (IntelliJ, Webstorm). Arguably the same features as Jenkins but it seems to be more Microsoft friendly.

###### [Bamboo](https://www.atlassian.com/software/bamboo/)

The CI system by Atlassian also delivers features for building and testing of software in a workflow. The strongpoint of this solution is that it ties in neatly with other Atlassian products like Jira and Confluence. It is based on Open Source tooling.

###### [Team foundation Server (TFS)](https://www.visualstudio.com/en-us/products/visual-studio-team-services-vs.aspx)

Microsoft’s entry is this market. It can handle not just building and testing alone but it offers release management and projectmanagement capabilities as well. It combines nicely with all Microsoft server products (of course). I used to vote against it because I don’t like the idea of vendor lock-in, but with the new wind blowing inside Microsoft introduced by their CEO Nadella and their embrace of Open Source.. Could work in a heterogeneous environment.


