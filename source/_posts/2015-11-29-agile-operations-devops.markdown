---
layout: post
title: "Agile operations == DEVOPS"
date: 2015-11-29 21:43:48
categories: agile cd ci devops
---

It has been around for some years but when I read about this concept last year I immediately felt comfy: [DEVOPS](https://en.wikipedia.org/wiki/DevOps). It’s actually two words and it means the marriage between IT development and IT operations.

![Screenshot of windows error](/images/2016/01/IMG_0207.jpg)
*Operations problem*

When these departments or people do not communicatie, bad things happen. I have seen this in person and felt it in person. I remember nightly visits to a datacenter in Rotterdam to reboot Unix boxes in the middle of the night (remember Girotel Online dutchies). What actually happens a lot is the existence of a disconnect between what is traditionally considered development work and what is traditionally considered operations work.

To smoothen the bond between DEV and OPS, for me it seems logical, as I started my career in IT systems administration and 15 years ago made a switch to the developer ‘camp’. Since those days I have never understood the difference between development and administration departments within big enterprises. I have worked in both worlds.  In my opinion we all try to reach the same end-goal, a happy customer.


