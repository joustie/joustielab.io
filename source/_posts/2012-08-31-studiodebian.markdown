---
layout: post
title: "studiodebian"
date: 2012-08-31 20:13:39
categories: dell linux pm800 windows
---

My work laptop has been running Windows 7 for a year. The funny thing is I never really used the OS for work. My development environment resides in a Virtualbox vm. The harddisk is a Samsung PM800 ssd and has been crawling to a halt since a couple of weeks. It has trim support and the firmware should support it(i checked it last year) but the write performance is … bad.. really REALLY bad. Unworkable. So I checked if a new firmware was available that deals with this issue..and yes!

So my ssd got wiped yesterday for the sake of speed. A good moment to evaluate one year of Windows 7. I decided to turn things around and run Debian as host and Windows varieties (when i need them) as vm guests.

Of course some issues (like broadcom drivers and some videocard stuff) but generally it runs fine.


