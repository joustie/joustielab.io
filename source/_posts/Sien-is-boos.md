---
title: Sien is boos
date: 2017-10-02 11:19:35
tags:
---

![Plaatje Sien is boos](/images/2017/10/MMt0VRs5wGp5hIMm.jpg)

Als je er op gaat letten, is het opvallend dat kinderboeken zo vol staan met stereotiepe rolpatronen. Stichting Zo-ook heeft hier onderzoek naar gedaan en wil kennis en bewustwording vergroten en ook het aanbod niet-stereotiepe kinderboeken vergroten.

Daarom zijn zij pas geleden met een [crowdfunding campagne](https://www.voordekunst.nl/projecten/5620-sien-is-boos) gestart om het uitgeven van een boek met niet-stereotiepe rolpatronen mogelijk te maken. Zij willen hiermee laten zien dat het ook anders kan. Niet met het argument dat het ene rolpatroon goed is en het andere slecht, maar om ervoor zorgen dat kinderen meerdere rolpatronen zien, dat ze andere rolmodellen zien. Dat ze zich kunnen inleven in iedereen, dat ze niet leren dat bepaalde activiteiten (of studies of banen) niet geschikt zijn voor de ene sekse, of dat de ene groep belangrijker is dan de andere.

Het boek Sien is boos, geschreven door schrijfster Liesbeth Mende, is net even anders dan anders, maar het ligt er niet dik bovenop. Het is vooral een aansprekend verhaal voor kinderen die net beginnen met lezen (Avi3 niveau).
Bij voldoende donateurs kan het worden gerealiseerd.

De campagne loopt nog twee weken. Het zou heel mooi zijn als het lukt!

Contactgegevens:
Carolien Jaspers
Stichting Zo-ook (Zo kan het ook!) voor diversiteit in kinderboeken
Website: [www.zo-ook.info](http://www.zo-ook.info)
Twitter: @zo_ook
