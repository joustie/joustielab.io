---
layout: post
title: "Part 1: Building a React App"
date: 2016-03-24 20:42:16
categories: react javascript agile cd jenkins nodejs cordova appium
---
Everyone knows Facebook, the biggest social-networking site. It is one of the largest sites of the planet. For years they have run their site on a customized version of PHP, named Hack. Not so long ago they switched to an internal product for parts of their site. This product is called React, it is a JavaScript framework. It’s star rose quickly internally.   

The crux of the product is that it is a component-based, decoupled way of developing software, which is based on the best ideas from computer science about good design.
  
The abstraction of the DOM (Document Object Model) is handled by using a Virtual Dom. This means that you don't directly touch the DOM of an HTML document, but rather a virtual DOM, wherein React then determines whether it will update the real DOM. There will be a diff of the change and then React will only find an update of the change position and update.  

Last year Facebook published React as Open Source Software and an active community appeared around it. Many large companies have embraced technology and an ecosystem of addons came into being.
React is pure Javascript programming but one can also utilize JSX, a form of XML which can be converted by build tools in pure Javascript. The advantage of this is that a component consists of tree structures which have attributes that can be displayed.

Flux is a design pattern that has emerged as the preferred way to handle data in you React apps. It supports the idea of one-way dataflow. Instead of two-databindings in patterns like MVC, Flux only updates the view in one way. Below is a simplification but it shows clearly the unidirectional flow.

![Flux diagram](/images/2016/03/flux.jpg)

There are several examples online but let’s take the one from Facebook, the TodoMVC application. It is featured in a tutorial found [here](https://facebook.github.io/flux/docs/todo-list.html#content). 

We are only going to build it and use part of it for a mobile app, so get the source using :

<pre>
$cd $HOME/tmp
$mkdir blogpost
$cd blogpost
$git clone https://github.com/facebook/flux.git
</pre>
For now that’s ok, well use the example from this source tree.

<pre>
$cd flux/examples/flux-todomvc

$npm install
#lots of installing here

</pre>
Let’s start the project as mentioned in the package.json and have watchify ‘watch’ the file for live changes.
<pre>
$npm start
> todomvc-flux@0.0.3 start ..../www
> watchify -o js/bundle.js -v -d js/app.js

2906584 bytes written to js/bundle.js (3.05 seconds)
</pre>

Fire up your browser and open the index.html located in www directory of the flux-todomvc example.

![todomvc screenshot](/images/2016/03/todomvcscreen.png)

Now open the file Header.react.js in www/js/components
`render: function() {
    return (
      <header id="header">
        <h1>todos</h1>
        <TodoTextInput
          id="new-todo"
          placeholder="What needs to be done?"
          onSave={this._onSave}
        />
      </header>
    );
  },
`  
and change the line that says:
` placeholder="What needs to be done?"`   
to
`  placeholder="What needs to be done, Joustie?"`

Now refresh the browser and you will get:
![todomvcscreenshot edited](/images/2016/03/todomvcscreen_edited.png)

To stop watchify 'watch' your live changes just go back to terminal/console/xterm shell session and hit control-c to signal quit.

So now we got this basic app running and I can start writing Part 2 of Running CI end-to-end testing on Saucelabs with a React hybrid cordova app. 

