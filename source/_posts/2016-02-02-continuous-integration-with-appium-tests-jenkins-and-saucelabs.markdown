---
layout: post
title: "Running CI end-to-end testing on Sauce Labs with a React hybrid cordova app"
date: 2016-02-02 19:21:20
categories: ci nodejs react javascript agile jenkins cordova mocha saucelabs appium
---
In the wonderful world of crossplatform App development it's hard to do end-to-end testing and CI. We have been able to setup CI for the CHECK web application, and I wanted to use the experience to make it work for mobile as well.

Those React mobile apps generally run in a Cordova container, not as a native app. Of course there is the React Native project, which will run directly through a javascript bridge, but for now we are still using Cordova.

Today I will show you from scatch how we go about this.

This delivery process consists of the following steps:

* Part 1
  *  building the React application (with flux as example)
* ~~Part 2~~ 
  *  ~~embed the application in a Cordova container~~  
  *  ~~prepare the container~~
  *  ~~build the Cordova container into a package that ~~
* Part 2  (Android) 
  *  embed the application in a Cordova container  
  *  prepare the container  
  * build the Cordova container into a package that can run in the Genymotion Android emulator
* Part 2 (iOS)
  *  embed the application in a Cordova container  
  *  prepare the container
  *  build the Cordova container into a package that can run in the iOS simulator  
* Part 3  
  *  upload the app package to Sauce labs  
  *  create the Appium test (with Mocha)  
  *  run the test on Sauce Labs  
